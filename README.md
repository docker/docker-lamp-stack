# docker lamp stack

This is a simple docker compose project of a LAMP stack.

Simply clone this repository, create a `.env` file from the `.env.example` and run `docker compose up -d` to start the container up.


## docker commands cheatsheet

- `docker compose up -d` - starts the containers
- `docker compose down` - stops the containers
- `docker compose down -v` stops the containers and removes the containers' persistent volumes
- `docker compose build` - builds the `web` container from the `Dockerfile`.
- `docker compose exec web bash` - opens a new `bash` prompt in the web docker container
- `docker compose exec database mysql -uroot -proot` - open a new interactive `mysql` prompt to interact with the database.